.PHONY: dist clean upload tag help

help:
	@echo "See Makefile itself for help"

clean:
	rm -r hektor.egg-info dist build __pycache__

dist:
	pip install -U setuptools pip wheel && \
	python setup.py bdist_wheel --universal

upload: dist
	twine upload dist/*

tag:
	git tag `python setup.py --version`
