# New modules need to be registered here

from lib.generic import Converter  # noqa
from lib.qti import QTIConverter  # noqa
from lib.xls import XLSConverter  # noqa
from lib.identity import JSONIdentityConverter  # noqa
